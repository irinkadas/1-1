public class Phone {
    private String name;
    private String model;
    private String storageVolum;
    private String storageRAM;
    private String number;

    Phone(String name, String model, String storageVolum, String storageRAM, String number) {
        this.name = name;
        this.model = model;
        this.storageVolum = storageVolum;
        this.storageRAM = storageRAM;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public String getStorageVolum() {
        return storageVolum;
    }

    public String getStorageRAM() {
        return storageRAM;
    }

    public String getNumber() {
        return number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setStorageVolum(String storageVolum) {
        this.storageVolum = storageVolum;
    }

    public void setStorageRAM(String storageRAM) {
        this.storageRAM = storageRAM;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}